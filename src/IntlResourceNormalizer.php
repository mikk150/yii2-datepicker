<?php

namespace mikk150\datepicker;

use ResourceBundle;
use yii\base\BaseObject;

class IntlResourceNormalizer extends BaseObject
{
    protected const LOCALE_REGEX = '/^(?<language>[a-z]{2,3})(?:[-_](?<script>[a-zA-Z]{4})){0,1}(?:[-_](?<country>[A-Z]{2})){0,1}/';


    public function getBundleData($locale)
    {
        $localeVariants = array_reverse($this->getLocaleVariants($locale));

        return new ResourceBundleArray(array_map(function ($localeVariant) {
            return ResourceBundle::create($localeVariant, null);
        }, $localeVariants));
    }

    protected function getLocaleVariants($locale)
    {
        $locales = [];
        if (preg_match(static::LOCALE_REGEX, $locale, $matches)) {
            if (!$this->isEmpty($matches, 'language') && !$this->isEmpty($matches, 'script') && !$this->isEmpty($matches, 'country')) {
                $locales[] = $matches['language'] . '-' . ucfirst(strtolower($matches['script'])) . '-' . $matches['country'];
            }
            if (!$this->isEmpty($matches, 'language') && !$this->isEmpty($matches, 'script')) {
                $locales[] = $matches['language'] . '-' . ucfirst(strtolower($matches['script']));
            }
            if (!$this->isEmpty($matches, 'language')) {
                $locales[] = $matches['language'];
            }
        }
        return $locales;
    }

    private function isEmpty($matches, $element)
    {
        return !array_key_exists($element, $matches) || $matches[$element] === null || $matches[$element] === [] || $matches[$element] === '';
    }
}
