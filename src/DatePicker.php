<?php

namespace mikk150\datepicker;

use IntlCalendar;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

class DatePicker extends InputWidget
{
    public $datePickerOptions = [];

    public $locale;

    public $titleFormat;

    public $format;

    public $dataFormat;

    public function init()
    {
        if (!$this->locale) {
            $this->locale = Yii::$app->language;
        }

        if (!$this->titleFormat) {
            $this->titleFormat = 'MM yyyy';
        }

        if (!$this->dataFormat) {
            $this->dataFormat = 'yyyy-mm-dd';
        }

        if (!$this->format) {
            $this->format = FormatConverter::convertFromIcuToDatepickerFormat(Yii::$app->formatter->dateFormat, $this->locale);
        }

        if (!isset($this->datePickerOptions['language'])) {
            $this->datePickerOptions['language'] = $this->locale;
        }

        parent::init();
    }

    /**
     * @inheritdoc
     * @throws ReflectionException
     * @throws InvalidConfigException
     */
    public function run()
    {
        parent::run();
        return $this->renderWidget();
    }

    protected function renderWidget()
    {
        $output = '';
        $this->registerLocale();
        $this->registerAsset();
        $this->registerJavascript();
        $output .= Html::input('input', '', $this->value, ArrayHelper::merge($this->options, [
            'id' => $this->options['id'] . '-picker',
        ]));
        $output .= $this->renderInputHtml('hidden');
        return $output;
    }

    protected function registerAsset()
    {
        DatePickerAsset::register($this->view);
    }

    public function registerLocale()
    {
        $normalizer = new IntlResourceNormalizer();

        $bundleData = $normalizer->getBundleData($this->locale);

        $calendar = IntlCalendar::createInstance(null, $this->locale);

        $calendarBundleData = ArrayHelper::getValue($bundleData, 'calendar.gregorian');

        $monthBundleData = ArrayHelper::getValue($calendarBundleData, 'monthNames');
        $dayBundleData = ArrayHelper::getValue($calendarBundleData, 'dayNames');

        $languageArray = [
            'days' => ArrayHelper::getValue($dayBundleData, 'format.wide'),
            'daysShort' => ArrayHelper::getValue($dayBundleData, 'format.abbreviated'),
            'daysMin' => ArrayHelper::getValue($dayBundleData, 'stand-alone.narrow'),
            'months' => ArrayHelper::getValue($monthBundleData, 'format.wide'),
            'monthsShort' => ArrayHelper::getValue($monthBundleData, 'format.abbreviated'),
            'today' => 'Today',
            'clear' => 'Clear',
            'format' => $this->format,
            'weekStart' =>  $calendar->getFirstDayOfWeek() - 1,
            'titeFormat' => $this->titleFormat,
        ];

        $locale = $this->locale;
        $languageData = Json::htmlEncode($languageArray);
        $this->view->registerJs(<<<JAVASCRIPT
        $.fn.datepicker.dates['${locale}'] = $languageData;
JAVASCRIPT
        );
    }

    protected function registerJavascript()
    {
        $formElement = '#' . $this->options['id'];
        $pickerElement = '#' . $this->options['id'] . '-picker';
        
        $pickerOptions = Json::htmlEncode($this->datePickerOptions);
        $dataFormat = $this->dataFormat;

        $locale = $this->locale;

        $this->view->registerJs(<<<JAVASCRIPT
        (function ($) {
            var instanceDate = new Date($("${formElement}").val());
            var datePicker = $("${pickerElement}").datepicker($pickerOptions).on('changeDate', function(event) {
                $("${formElement}").val(event.format(null, '${dataFormat}')).trigger('change');
            });
            $("${pickerElement}").datepicker('setDate', $.fn.datepicker.DPGlobal.parseDate(instanceDate, '${dataFormat}' , '${locale}', true));
        })(jQuery);
JAVASCRIPT
        );
    }
}
