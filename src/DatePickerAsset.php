<?php

namespace mikk150\datepicker;

use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class DatePickerAsset extends AssetBundle
{
    public $sourcePath = '@bower/bootstrap-datepicker/dist';

    public $depends = [
        BootstrapAsset::class,
        JqueryAsset::class
    ];

    public $js = [
        'js/bootstrap-datepicker.min.js'
    ];

    public $css = [
        'css/bootstrap-datepicker3.min.css'
    ];
}
