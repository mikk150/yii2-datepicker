<?php

namespace mikk150\datepicker;

use ArrayAccess;
use BadFunctionCallException;
use Iterator;
use ResourceBundle;

class ResourceBundleArray implements ArrayAccess, Iterator
{
    private $bundleArrays;

    private $position = 0;

    private $keys;

    public function __construct($bundles)
    {
        $this->bundleArrays = array_map('iterator_to_array', $bundles);

        $this->keys = array_unique(array_reduce($this->bundleArrays, function ($carry, $bundleArray) {
            return array_merge($carry, array_keys($bundleArray));
        }, []));
    }

    public function offsetExists($offset)
    {
        $resourceBundles = [];

        foreach ($this->bundleArrays as $bundleArray) {
            if (isset($bundleArray[$offset])) {
                return true;
            }
        }
        return false;
    }

    public function offsetGet($offset)
    {
        if (!$this->offsetExists($offset)) {
            return null;
        }
        $resourceBundles = [];

        foreach ($this->bundleArrays as $bundleArray) {
            if (isset($bundleArray[$offset])) {
                if ($bundleArray[$offset] instanceof ResourceBundle) {
                    $resourceBundles[] = $bundleArray[$offset];
                    continue;
                }
                return $bundleArray[$offset];
            }
        }
        return new self($resourceBundles);
    }

    public function offsetSet($offset, $value)
    {
        throw new BadFunctionCallException('Cannot set elements to ResourceBundle');
    }

    public function offsetUnset($offset)
    {
        throw new BadFunctionCallException('Cannot remove elements to ResourceBundle');
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->offsetGet($this->keys[$this->position]);
    }

    public function key()
    {
        return $this->keys[$this->position];
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        if (!isset($this->keys[$this->position])) {
            return false;
        }
        return $this->offsetExists($this->keys[$this->position]);
    }
}
