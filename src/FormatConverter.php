<?php

namespace mikk150\datepicker;

use IntlDateFormatter;

class FormatConverter
{
    private static $_icuShortFormats = [
        'short' => 3, // IntlDateFormatter::SHORT,
        'medium' => 2, // IntlDateFormatter::MEDIUM,
        'long' => 1, // IntlDateFormatter::LONG,
        'full' => 0, // IntlDateFormatter::FULL,
    ];

    public static function convertFromIcuToDatepickerFormat($pattern, $locale = null)
    {
        $formatter = new IntlDateFormatter($locale, self::$_icuShortFormats[$pattern], IntlDateFormatter::NONE);

        $pattern = $formatter->getPattern();

        // http://userguide.icu-project.org/formatparse/datetime#TOC-Date-Time-Format-Syntax
        // escaped text
        $escaped = [];
        if (preg_match_all('/(?<!\')\'(.*?[^\'])\'(?!\')/', $pattern, $matches, PREG_SET_ORDER)) {
            foreach ($matches as $match) {
                $match[1] = str_replace('\'\'', '\'', $match[1]);
                $escaped[$match[0]] = '\\' . implode('\\', preg_split('//u', $match[1], -1, PREG_SPLIT_NO_EMPTY));
            }
        }

        return strtr($pattern, array_merge($escaped, [
            'd' => 'd',
            'dd' => 'dd',
            'ccc' => 'D',
            'cccc' => 'DD',
            'M' => 'm',
            'MM' => 'mm',
            'MMM' => 'M',
            'MMMM' => 'MM',
            'yyyy' => 'yyyy',
            'y' => 'yyyy',
            'yy' => 'yy',
        ]));
    }
}
